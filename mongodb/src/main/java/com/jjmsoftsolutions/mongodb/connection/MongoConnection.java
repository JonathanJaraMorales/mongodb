package com.jjmsoftsolutions.mongodb.connection;

import java.net.UnknownHostException;
import java.util.List;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.DB;
import com.mongodb.MongoClient;

public class MongoConnection {

	private static MongoClient mongoClient = null;
	
	private MongoOperations mongoOps = null;
	private String host;
	private int port;
	private String dataBase;
	private String userName;
	private String password;
	private boolean authorized;

	private MongoConnection() {
	}

	public MongoClient getInstance(String host, int port, String dataBase, String userName, String password) {
		if (mongoClient == null) {
			try {
				setConnection(host, port,dataBase, userName, password);
				mongoClient = new MongoClient(host, port);
				mongoOps = new MongoTemplate(mongoClient, dataBase);
				DB db = mongoClient.getDB(dataBase);
				authorized = db.authenticate(userName, password.toCharArray());
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
		return mongoClient;
	}

	private void setConnection(String host, int port, String dataBase, String userName, String password) {
		this.host = host;
		this.port = port;
		this.dataBase = dataBase;
		this.userName = userName;
		this.password = password;
		this.authorized = false;
	}

	public List<String> getAllDataBases() {
		return mongoClient.getDatabaseNames();
	}
	
	public boolean insert(Object object){
		mongoOps.insert(object, object.getClass().getSimpleName());
		
		return false;
	}

}
